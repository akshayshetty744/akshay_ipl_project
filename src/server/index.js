const csv = require('csvtojson')
const fs = require("fs");

const csvFilePath1 = "../data/deliveries.csv";
const csvFilePath2 = "../data/matches.csv";
const { matches_played, match_won, extras, top10 } = require("./ipl");

csv()
  .fromFile(csvFilePath1)
  .then((jsonObj) => {
    fs.writeFile(
      "../public/output/deliveries.json",
      JSON.stringify(jsonObj, null, 4),
      (err) => {
        if (err) {
          throw err;
        }
      }
    );
  });
csv()
  .fromFile(csvFilePath2)
  .then((jsonObj) => {
    fs.writeFile(
      "../public/output/matches.json",
      JSON.stringify(jsonObj, null, 4),
      (err) => {
        if (err) {
          throw err;
        }
      }
    );
  });

const matches = require("../public/output/matches.json");
const deliveries = require("../public/output/deliveries.json");
const { match } = require("assert");

const macthes_per_season = matches_played(matches);
fs.writeFile(
  "../public/output/matches_per_season_des.json",
  JSON.stringify(macthes_per_season, null, 4),
  (err) => {
    if (err) {
      throw err;
    } else {
      console.log("matches_per_season_des.json file added succesfully");
    }
  }
);
const matchesWonPerTeamPerYearOP = match_won(matches);
fs.writeFile(
  "../public/output/matches_won_per_season_per_team_des.json",
  JSON.stringify(matchesWonPerTeamPerYearOP, null, 4),
  (err) => {
    if (err) {
      throw err;
    } else {
      console.log(
        "matches_won_per_season_per_team_des.json file added succesfully"
      );
    }
  }
);

fs.writeFile(
  "../public/output/extra_runs_per_team_2016.json",
  JSON.stringify(extras(matches, deliveries, 2016), null, 4),
  (err) => {
    if (err) {
      throw err;
    } else {
      console.log("extra_runs_per_team_2016.json file added succesfully");
    }
  }
);

fs.writeFile(
  "../public/output/extra_runs_per_team_2016.json",
  JSON.stringify(top10(matches, deliveries, 2016), null, 4),
  (err) => {
    if (err) {
      throw err;
    } else {
      console.log("extra_runs_per_team_2016.json file added succesfully");
    }
  }
);
