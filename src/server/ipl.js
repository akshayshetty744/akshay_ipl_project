//1. Number of matches played per year for all the years in IPL.
function matches_played(matches) {
  return matches
    .map((e) => e.season)
    .reduce((result, e) => {
      result[e] ? result[e]++ : (result[e] = 1);
      return result;
    }, {});
}

// 2. Number of matches won per team per year in IPL
function match_won(matches) {
  return matches.reduce((result, e) => {
    let x = e.season;
    let y = e.winner;
    !result[x]
      ? (result[x] = {})
      : result[x][y]
      ? result[x][y]++
      : (result[x][y] = 1);
    return result;
  }, {});
}

// 3. Extra runs conceded per team in the year 2016
function extras(deliveries, matches, year) {
  return deliveries.reduce((result, e) => {
    let bowling_team = e.bowling_team;
    let extraRuns = +e.extra_runs;
    matches
      .filter((element) => element.season == year)
      .map((ele) => ele.id)
      .includes(e.match_id)
      ? result[bowling_team]
        ? (result[bowling_team] += extraRuns)
        : (result[bowling_team] = extraRuns)
      : null;
    return result;
  }, {});
}

//4. Top 10 economical bowlers in the year 2015

function top10(matches, deliveries, year) {
  let bowlerObj = {};
  let matchID = [];
  matches.map((e) => {
    if (e.season == year) {
      matchID.push(e.id);
    }
  });
  deliveries.map((e) => {
    if (matchID.includes(e.match_id)) {
      let bowler = e.bowler;
      if (!bowlerObj[bowler]) {
        bowlerObj[bowler] = {};
      }
      bowlerObj[bowler]["runs"] =
        bowlerObj[bowler]["runs"] + parseInt(e.total_runs) ||
        parseInt(e.total_runs);
      bowlerObj[bowler]["balls"] = bowlerObj[bowler]["balls"] + 1 || 1;
      bowlerObj[bowler]["economy"] =
        bowlerObj[bowler]["runs"] / (bowlerObj[bowler]["balls"] / 6);
    }
  });

  let out = [];
  for (let key in bowlerObj) {
    out.push([bowlerObj[key]["economy"], key]);
  }
  out.sort((a, b) => a[0] - b[0]);
  let top10Bowlers = out.slice(0, 10);
  let obj = [];
  top10Bowlers.forEach((e) => {
    obj.push({ player_name: e[1], economy: Math.round(e[0]) });
  });
  return obj;
}

module.exports = { matches_played, match_won, extras, top10 };
